#!/bin/bash

set -x

WORKSPACE=$(pwd)/gst-validate/workspace
RESULTS=${WORKSPACE}/gst-validate-results.xml

. /opt/gstreamer-1.0/share/gstreamer/gst-env

# Setting TERM to linux prevents strange encoding issues related to unicode.
#
# PYTHONIOENCODING prevents gst-validate-launcher from failing since some of the
# media assets contain unicode information when the program is expecting ascii.
# This is most likely due to setting TERM to linux.
#
# Specifying SSL_CERT_DIR prevents a SSL certificate verification error
TERM=linux \
PYTHONIOENCODING=utf-8 \
SSL_CERT_DIR="/etc/ssl/certs/" \
gst-validate-launcher valdiate.file.glvideomixer.* --no-color --mute --jobs 2 \
    -M ${WORKSPACE}/gst-validate \
    --xunit-file ${RESULTS}

lava-test-case gst-validate-launcher --result pass

if [ -f ${RESULTS} ]
then
    lava-test-case push-results --shell "curl -X POST -d \"@${RESULTS}\" ${CALLBACK_URL}"
else
    echo "No results file found from gst-validate-launcher"
    lava-test-case push-results --result fail
    lava-test-case notify-jenkins --shell "curl -X POST -d '[FAILURE] COULD NOT RETRIEVE RESULTS' ${CALLBACK_URL}"
fi
