#!/bin/sh

set -x

ARTIFACT_URL=http://images.collabora.co.uk/gstreamer-rpi/test/latest-build
GST_VALIDATE_SYNC_URL=http://people.collabora.co.uk/~rakko/gst-validate-launcher-sync.tar.gz
INSTALL_DIR=/opt/gstreamer-1.0
WORKSPACE="$(pwd)/gst-validate/workspace"

mkdir -p ${WORKSPACE}

wget --no-check-certificate --progress=dot:giga --directory-prefix=${WORKSPACE} ${ARTIFACT_URL}
wget --no-check-certificate --progress=dot:giga --directory-prefix=${WORKSPACE} ${GST_VALIDATE_SYNC_URL}

mkdir -p ${INSTALL_DIR}
chmod 777 -R ${INSTALL_DIR}

tar xjf ${WORKSPACE}/latest-build -C ${INSTALL_DIR} --warning=no-timestamp
tar xzf ${WORKSPACE}/gst-validate-launcher-sync.tar.gz -C ${WORKSPACE} --warning=no-timestamp

